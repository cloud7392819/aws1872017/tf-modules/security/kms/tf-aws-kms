
data "aws_caller_identity" "current" {}

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_group" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test" {
  name = "${local.prefix}-test"
}

resource "aws_iam_user" "test2" {
  name = "${local.prefix}-test2"
}

resource "aws_iam_role" "test" {
  name = "${local.prefix}-test"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = { AWS = local.caller_arn }
      },
    ]
  })
}

resource "aws_kms_key" "example" {
  description = "example key for tests"
}

#####
# Default
#####

data "aws_iam_policy_document" "additional" {
  statement {
    sid = "Additional"
    actions = [
      "kms:DescribeKey",
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    resources = ["*"]
    condition {
      test     = "StringLike"
      values   = ["arn:aws:iam::255218212493:role/AdminRole"]
      variable = "aws:principalArn"
    }
  }
}

module "default" {
  source = "../../"

  kms_keys = {
    one = {
      alias       = "${local.prefix}example-one"
      policy_json = data.aws_iam_policy_document.additional.json
      principal_actions = {
        ro = [
          "kms:Decrypt",
          "kms:GenerateDataKey",
        ]
        rw = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        rwd = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:RetireGrant",
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        full = [
          "kms:CreateGrant",
          "kms:DescribeKey",
          "kms:RetireGrant",
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
      }
      service_principal = "secretsmanager"
      service_actions = [
        "kms:CreateGrant",
        "kms:GenerateDataKey",
        "kms:Decrypt",
      ]
      description = "An example of KMS key for S3"
    }

    two = {
      alias = "${local.prefix}example-two"
      principal_actions = {
        ro = [
          "kms:Decrypt",
          "kms:DescribeKey",
        ]
        rw = [
          "kms:DescribeKey",
          "kms:Decrypt",
          "kms:Encrypt",
        ]
        rwd = [
          "kms:DescribeKey",
          "kms:Decrypt",
          "kms:Encrypt",
        ]
        full = [
          "kms:DescribeKey",
          "kms:Decrypt",
          "kms:Encrypt",
        ]
      }
      service_principal = "s3"
      service_actions   = ["kms:Decrypt"]
      description       = "An example of KMS key for Secrets Manager"
      rotation_enabled  = false
      tags = {
        additional = "value"
      }
    }
  }
  replica_enabled = true

  iam_admin_arns = { 0 = local.caller_arn }
  iam_policy_entity_arns = {
    ro = [aws_iam_user.test.arn], rw = [aws_iam_user.test2.arn, aws_iam_role.test.arn]
  }
  iam_policy_sid_prefix              = local.prefix
  iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]

  tags = {}

  providers = {
    aws.replica = aws.replica
  }
}

#####
# IAM
#####

module "iam" {
  source = "../../"
  kms_keys = {
    iam = {
      alias = "${local.prefix}example-iam"
      principal_actions = {
        ro = [
          "kms:Decrypt",
        ]
        rw = [
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        rwd = [
          "kms:GenerateDataKey",
          "kms:Decrypt",
        ]
        full = [
          "kms:*",
        ]
      }
      service_principal = "ecr"
      service_actions = [
        "kms:CreateGrant",
        "kms:GenerateDataKey",
        "kms:Decrypt",
      ]
      description = "An example of KMS key for no ECR"
    }
  }

  iam_admin_arns         = { 0 = local.caller_arn }
  iam_policy_entity_arns = { ro = [aws_iam_user.test.arn], rw = [aws_iam_user.test2.arn, aws_iam_role.test.arn] }
  iam_policy_sid_prefix  = local.prefix

  iam_policy_export_json = true
  iam_policy_external_actions_ro = [
    "kms:Decrypt",
  ]
  iam_policy_external_actions_rw = [
    "kms:Decrypt",
    "kms:GenerateDataKey",
  ]
  iam_policy_create        = true
  iam_policy_name_template = "${local.prefix}kmspolicy-%s"
  iam_policy_description   = "A test policy to access ECR KMS"
  iam_policy_external_resource_arns = {
    0 = aws_kms_key.example.arn
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Minimal
#####

module "minimal" {
  source = "../../"

  kms_keys = {
    minimal = {
      alias = "${local.prefix}example-minimal"
      principal_actions = {
        ro = [
          "kms:Decrypt",
        ]
        rw = [
          "kms:Decrypt",
        ]
        rwd = [
          "kms:Decrypt",
        ]
        full = [
          "kms:*",
        ]
      }
      description = "An example of KMS key for no service in particular"
    }
  }

  replica_enabled = false

  iam_admin_arns = { 0 = local.caller_arn }

  providers = {
    aws.replica = aws.replica
  }
}

