#####
# Default
#####

data "aws_kms_key" "default_kms" {
  for_each = module.default.kms_key

  key_id = each.value.id

  depends_on = [
    module.default
  ]
}

check "default" {
  assert {
    condition = (
      module.default.kms_key.one.multi_region == true &&
      module.default.kms_key.one.multi_region_replica_id == module.default.kms_key.one.id &&
      module.default.kms_key.one.multi_region_replica_alias_id == module.default.kms_key.one.alias_id &&
      module.default.kms_key.one.multi_region_replica_arn == data.aws_kms_key.default_kms["one"].multi_region_configuration[0].replica_keys[0].arn &&
      module.default.kms_key.two.multi_region == true &&
      module.default.kms_key.two.multi_region_replica_id == module.default.kms_key.two.id &&
      module.default.kms_key.two.multi_region_replica_alias_id == module.default.kms_key.two.alias_id &&
      module.default.kms_key.two.multi_region_replica_arn == data.aws_kms_key.default_kms["two"].multi_region_configuration[0].replica_keys[0].arn
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      module.default.kms_key_iam_policy == null &&
      module.default.kms_key_iam_policy_jsons == null
    )
    error_message = "KMS functional test fails: external policy error"
  }
}

#####
# IAM
#####

check "iam" {
  assert {
    condition = (
      module.minimal.kms_key.minimal.multi_region == false &&
      module.minimal.kms_key.minimal.multi_region_replica_id == null
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      contains(jsondecode(module.iam.kms_key_iam_policy_jsons.ro)["Statement"][0]["Resource"], aws_kms_key.example.id) &&
      contains(jsondecode(module.iam.kms_key_iam_policy_jsons.ro)["Statement"][0]["Resource"], module.iam.kms_key.iam.alias_arn) &&
      length(jsondecode(module.iam.kms_key_iam_policy_jsons.rw)["Statement"][0]["Action"]) == 2 &&
      contains(jsondecode(module.iam.kms_key_iam_policy_jsons.rw)["Statement"][0]["Action"], "kms:GenerateDataKey")
    )
    error_message = "KMS functional test fails: external policy document error"
  }

  assert {
    condition = (
      length(module.iam.kms_key_iam_policy) == 2 &&
      module.iam.kms_key_iam_policy.ro.name == "${local.prefix}kmspolicy-ro" &&
      module.iam.kms_key_iam_policy.rw.name == "${local.prefix}kmspolicy-rw"
    )
    error_message = "KMS functional test fails: external policy error"
  }


}

#####
# Minimal
#####

check "minimal" {
  assert {
    condition = (
      module.minimal.kms_key.minimal.multi_region == false &&
      module.minimal.kms_key.minimal.multi_region_replica_id == null
    )
    error_message = "KMS functional test fails: replica error"
  }

  assert {
    condition = (
      module.minimal.kms_key_iam_policy == null &&
      module.minimal.kms_key_iam_policy_jsons == null
    )
    error_message = "KMS functional test fails: external policy error"
  }
}

