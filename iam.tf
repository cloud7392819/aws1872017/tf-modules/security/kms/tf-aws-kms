
####
# Variables
####

variable "iam_policy_export_json" {
  description = "Whether to export KMS IAM policies as JSON strings."
  type        = bool
  default     = false
}

variable "iam_policy_external_actions_ro" {
  description = "List of read-only KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default     = null
}

variable "iam_policy_external_actions_rw" {
  description = "List of read-write KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default     = null
}

variable "iam_policy_external_actions_rwd" {
  description = "List of read-write-delete KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage."
  type        = list(string)
  default     = null
}

variable "iam_policy_external_actions_full" {
  description = "List of admin KMS actions that could be allowed for external IAM entities. Most likely similar to `var.iam_policy_external_actions_rwd`."
  type        = list(string)
  default     = null
}

variable "iam_policy_external_resource_arns" {
  description = "ARNs of KMS keys or aliases created outside this module to compose the external policies with. Along with KMS keys created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values."
  type        = map(string)
  default     = null

  validation {
    condition = ((!contains([
      for arn in coalesce(var.iam_policy_external_resource_arns, {}) :
      (arn == null || can(regex("^arn:aws(-us-gov|-cn)?:kms:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):(key|alias)/[0-9A-Za-z\\/_-]{2,256}$", arn)))
    ], false)))
    error_message = "One or more “var.iam_policy_external_resource_arns” are invalid."
  }
}

variable "iam_policy_create" {
  description = "Whether to generate external policies. For more complex use cases, this can be toggled off in favor of `var.iam_policy_export_json`."
  type        = bool
  default     = false
}

variable "iam_policy_name_template" {
  type        = string
  description = "Template for the name of the policies to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. “%s” should be somewhere inside the variable and will be replaced by the scope of the policy (`ro`, `rw`, `rwd` or `full`)."
  default     = null

  validation {
    condition     = var.iam_policy_name_template == null || (can(regex("^[a-zA-Z0-9+=,\\.\\%/@-]+$", var.iam_policy_name_template)) && can(regex("\\%s", var.iam_policy_name_template)))
    error_message = "“var.iam_policy_name_template” is invalid."
  }
}

variable "iam_policy_path" {
  type        = string
  description = "Path of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`."
  default     = null

  validation {
    condition     = var.iam_policy_path == null || can(regex("^\\/.*$", var.iam_policy_path))
    error_message = "“var.iam_policy_path” is invalid."
  }
}

variable "iam_policy_description" {
  type        = string
  description = "Description of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`."
  default     = null

  validation {
    condition     = var.iam_policy_description == null || (1 <= length(coalesce(var.iam_policy_description, "empty")) && length(coalesce(var.iam_policy_description, "empty")) <= 1024)
    error_message = "“var.iam_policy_description” is invalid."
  }
}

variable "iam_policy_tags" {
  type        = map(string)
  default     = null
  description = "Tags of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`."
}

####
# Locals
####

locals {
  iam_export_json                     = var.iam_policy_export_json && local.kms_should_create
  iam_should_create_external_policy   = var.iam_policy_create && local.kms_should_create
  iam_should_create_external_document = local.iam_should_create_external_policy || local.iam_export_json
  iam_external_actions = {
    ro   = coalesce(var.iam_policy_external_actions_ro, [])
    rw   = coalesce(var.iam_policy_external_actions_rw, [])
    rwd  = coalesce(var.iam_policy_external_actions_rwd, [])
    full = coalesce(var.iam_policy_external_actions_full, [])
  }
}

####
# Data
####

data "aws_iam_policy_document" "external_kms" {
  for_each = local.iam_should_create_external_document ? local.iam_external_actions : {}

  statement {
    sid     = "${try(chomp(var.iam_policy_sid_prefix), "")}KMS${upper(each.key)}"
    effect  = "Allow"
    actions = each.value
    resources = compact(concat(
      flatten([for key, kms_key in aws_kms_key.this : [
        aws_kms_alias.this[key].arn,
        var.replica_enabled ? aws_kms_alias.replica[key].arn : null,
      ]]),
      [
        for arn in coalesce(var.iam_policy_external_resource_arns, {}) :
        arn
      ]
    ))
  }

  lifecycle {
    precondition {
      condition     = length(each.value) == 0 || (length(each.value) > 0 && contains(keys(var.iam_policy_entity_arns), each.key))
      error_message = "“${each.key}” was provided in var.iam_policy_entity_arns, but the corresponding var.iam_policy_external_actions_… is empty."
    }
  }
}

####
# Resources
####

resource "aws_iam_policy" "this" {
  for_each = local.iam_should_create_external_policy ? coalesce(var.iam_policy_entity_arns, {}) : {}

  name   = format(var.iam_policy_name_template, each.key)
  path   = coalesce(var.iam_policy_path, "/")
  policy = data.aws_iam_policy_document.external_kms[each.key].json

  description = var.iam_policy_description

  tags = merge(
    {
      Name        = format(var.iam_policy_name_template, each.key)
      Description = var.iam_policy_description
    },
    local.tags,
    var.iam_policy_tags,
  )
}

####
# Outputs
####

output "kms_key_iam_policy" {
  value = local.iam_should_create_external_policy ? { for key, policy in aws_iam_policy.this :
    key => {
      arn       = policy.arn
      name      = policy.name
      id        = policy.id
      policy_id = policy.policy_id
    }
  } : null
}

output "kms_key_iam_policy_jsons" {
  value = local.iam_export_json ? { for key, policy_data in data.aws_iam_policy_document.external_kms :
    key => policy_data.json
  } : null
}

