![Terraform](https://lgallardo.com/images/terraform.jpg)

# terraform-module-kms [tf-aws-kms]

Terraform module to create and manage KMS and related resources.

## Limitations

Feel free to make a pull request to circumvent these limitations.

- Each key can only be consumed by a single AWS service (e.g. `ec2`, `s3`, etc.).
- Internal IAM policy will grant *maximal* permissions for provided IAM entities, while `policy_json` can be used to further restrict KMS key accesses.
- Does not yet manage custom key stores (`aws_kms_custom_key_store`)
- Does not yet manage grants (`aws_kms_grant`)
- Does not yet manage external keys (`aws_kms_external_key`)

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.9 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.9 |
| <a name="provider_aws.replica"></a> [aws.replica](#provider\_aws.replica) | >= 5.9 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_kms_alias.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key_policy.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_key_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key_policy) | resource |
| [aws_kms_replica_key.replica](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_replica_key) | resource |
| [aws_iam_policy_document.external_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.internal_kms](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_iam_admin_arns"></a> [iam\_admin\_arns](#input\_iam\_admin\_arns) | ARNs of the Administrators for all the resources of this module. Wildcards are authorized. `var.iam_policy_attach_to_principals` will have no effect on these resources. Keys are free values. | `map(string)` | `null` | no |
| <a name="input_iam_policy_create"></a> [iam\_policy\_create](#input\_iam\_policy\_create) | Whether to generate external policies. For more complex use cases, this can be toggled off in favor of `var.iam_policy_export_json`. | `bool` | `false` | no |
| <a name="input_iam_policy_description"></a> [iam\_policy\_description](#input\_iam\_policy\_description) | Description of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `string` | `null` | no |
| <a name="input_iam_policy_entity_arns"></a> [iam\_policy\_entity\_arns](#input\_iam\_policy\_entity\_arns) | Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside. | `map(list(string))` | `null` | no |
| <a name="input_iam_policy_export_json"></a> [iam\_policy\_export\_json](#input\_iam\_policy\_export\_json) | Whether to export KMS IAM policies as JSON strings. | `bool` | `false` | no |
| <a name="input_iam_policy_external_actions_full"></a> [iam\_policy\_external\_actions\_full](#input\_iam\_policy\_external\_actions\_full) | List of admin KMS actions that could be allowed for external IAM entities. Most likely similar to `var.iam_policy_external_actions_rwd`. | `list(string)` | `null` | no |
| <a name="input_iam_policy_external_actions_ro"></a> [iam\_policy\_external\_actions\_ro](#input\_iam\_policy\_external\_actions\_ro) | List of read-only KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | `null` | no |
| <a name="input_iam_policy_external_actions_rw"></a> [iam\_policy\_external\_actions\_rw](#input\_iam\_policy\_external\_actions\_rw) | List of read-write KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | `null` | no |
| <a name="input_iam_policy_external_actions_rwd"></a> [iam\_policy\_external\_actions\_rwd](#input\_iam\_policy\_external\_actions\_rwd) | List of read-write-delete KMS actions that could be allowed for external IAM entities. This should be the bare minimum according to the intended key usage. | `list(string)` | `null` | no |
| <a name="input_iam_policy_external_resource_arns"></a> [iam\_policy\_external\_resource\_arns](#input\_iam\_policy\_external\_resource\_arns) | ARNs of KMS keys or aliases created outside this module to compose the external policies with. Along with KMS keys created inside this module, the given ARNs will be merged into external IAM policies created by this module. Keys are free values. | `map(string)` | `null` | no |
| <a name="input_iam_policy_name_template"></a> [iam\_policy\_name\_template](#input\_iam\_policy\_name\_template) | Template for the name of the policies to attach to the given principals. Only valid if `var.iam_policy_attach_to_principals` is `true`. “%s” should be somewhere inside the variable and will be replaced by the scope of the policy (`ro`, `rw`, `rwd` or `full`). | `string` | `null` | no |
| <a name="input_iam_policy_path"></a> [iam\_policy\_path](#input\_iam\_policy\_path) | Path of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `string` | `null` | no |
| <a name="input_iam_policy_restrict_by_account_ids"></a> [iam\_policy\_restrict\_by\_account\_ids](#input\_iam\_policy\_restrict\_by\_account\_ids) | Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards. | `list(string)` | `null` | no |
| <a name="input_iam_policy_sid_prefix"></a> [iam\_policy\_sid\_prefix](#input\_iam\_policy\_sid\_prefix) | Use a prefix for all `Sid` of all the policies - internal and external - created by this module. | `string` | `null` | no |
| <a name="input_iam_policy_tags"></a> [iam\_policy\_tags](#input\_iam\_policy\_tags) | Tags of the policy to attach to the given `var.iam_policy_entity_arns`. Only valid if `var.iam_policy_attach_to_principals` is `true`. | `map(string)` | `null` | no |
| <a name="input_kms_keys"></a> [kms\_keys](#input\_kms\_keys) | Options of the KMS key to create.<br>If given and and `var.replica_enabled`, this key will be use also for the replica, unless `var.replica_kms_key_id` is set.<br>If given, `var.kms_key_id` will be ignored.<br><br>  * alias                   (required, string):                      KMS key alias; Display name of the KMS key. Omit the `alias/`.<br>  * principal\_actions       (required, map(list(string))):           List of read-only, read-write, read-write-delete and full allowed IAM actions for the principals defined by "var.iam\_policy\_entity\_arns[key]" variable, or nobody, if variable value is undefined. The four expected keys are "ro", "rw", "rwd" and "full".<br>  * policy\_json             (optional, string):                      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam\_admin\_arns" to avoid getting blocked.<br>  * service\_principal       (optional, string):                      The name of the service authorized to access the key.<br>  * service\_actions         (optional, list(string)):                List of allowed IAM actions for the "service\_principal" authorized to access the key.<br>  * description             (optional, string, ""):                  Description of the key.<br>  * usage                   (optional, string, "ENCRYPT\_DECRYPT"):   Specifies the intended use of the key. Valid values: `ENCRYPT_DECRYPT`, `SIGN_VERIFY`, or `GENERATE_VERIFY_MAC`.<br>  * spec                    (optional, string, "SYMMETRIC\_DEFAULT"): Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `HMAC_256`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`.<br>  * rotation\_enabled        (optional, bool, true):                  Whether to automatically rotate the KMS key linked to the secrets.<br>  * deletion\_window\_in\_days (optional, number, 7):                   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.<br>  * tags                    (optional, map(string)):                 Tags to be used by the KMS key of this module. | <pre>map(object({<br>    alias                   = string<br>    principal_actions       = map(list(string))<br>    policy_json             = optional(string, null)<br>    service_principal       = optional(string, null)<br>    service_actions         = optional(list(string), null)<br>    description             = optional(string, "")<br>    usage                   = optional(string, "ENCRYPT_DECRYPT")<br>    spec                    = optional(string, "SYMMETRIC_DEFAULT")<br>    rotation_enabled        = optional(bool, true)<br>    deletion_window_in_days = optional(number, 7)<br>    tags                    = optional(map(string), {})<br>  }))</pre> | `null` | no |
| <a name="input_replica_enabled"></a> [replica\_enabled](#input\_replica\_enabled) | If `true`, KMS will be replicated into the `aws.replica` region. | `bool` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be shared with all resources of this module. | `map(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_kms_key"></a> [kms\_key](#output\_kms\_key) | n/a |
| <a name="output_kms_key_iam_policy"></a> [kms\_key\_iam\_policy](#output\_kms\_key\_iam\_policy) | n/a |
| <a name="output_kms_key_iam_policy_jsons"></a> [kms\_key\_iam\_policy\_jsons](#output\_kms\_key\_iam\_policy\_jsons) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.


## URL

1. https://gitlab.com/wild-beavers/terraform/module-aws-kms/

